import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.tehnicendolg.rest.GetFromRest;
import com.tehnicendolg.vnos.ProjektTipVnosa;
import org.json.JSONObject;

import java.awt.*;
import java.util.ArrayList;

public class Main extends AnAction {


    public Main() {
        super("Dodaj dolg");
    }

    public void actionPerformed(AnActionEvent event) {

        GetFromRest get = new GetFromRest();

        String vsi_projekti = "http://localhost:8080/projekts/";

        ArrayList<String[]> listaMojihProjektov = new ArrayList<>();

        Project project = event.getProject();

        String uporabnisko_ime = Messages.showInputDialog(project, "Vnesite elektronski naslov s katerim se vpi\u0161ete v spletno platformo tehni\u010Den dolg:",
                "Uporabni\u0161ko ime", Messages.getQuestionIcon());

        if(uporabnisko_ime != null && !uporabnisko_ime.isEmpty() ) {

            for (int i = 0; i < get.pridobi_podatke(vsi_projekti).length(); i++) {

                JSONObject obj = get.pridobi_podatke(vsi_projekti).getJSONObject(i);

                String clan = "", clan1 = "", clan2 = "", clan3 = "", clan4 = "", clan5 = "", clan6 = "", clan7 = "", clan8 = "", clan9 = "";

                if (!obj.isNull("clan")) {
                    clan = obj.getString("clan");
                }
                if (!obj.isNull("clan1")) {
                    clan1 = obj.getString("clan1");
                }
                if (!obj.isNull("clan2")) {
                    clan2 = obj.getString("clan2");
                }
                if (!obj.isNull("clan3")) {
                    clan3 = obj.getString("clan3");
                }
                if (!obj.isNull("clan4")) {
                    clan4 = obj.getString("clan4");
                }
                if (!obj.isNull("clan5")) {
                    clan5 = obj.getString("clan5");
                }
                if (!obj.isNull("clan6")) {
                    clan6 = obj.getString("clan6");
                }
                if (!obj.isNull("clan7")) {
                    clan7 = obj.getString("clan7");
                }
                if (!obj.isNull("clan8")) {
                    clan8 = obj.getString("clan8");
                }
                if (!obj.isNull("clan9")) {
                    clan9 = obj.getString("clan9");
                }
                String id = obj.getString("_id");
                String naziv = obj.getString("naziv");
                String tip_ocena = obj.getString("tip_ocena");

                String[] seznamPodatkovProjekta = {id, naziv, tip_ocena, clan, clan1, clan2, clan3, clan4, clan5,
                                                    clan6, clan7, clan8, clan9};

                if (uporabnisko_ime.equals(clan)) {
                    listaMojihProjektov.add(seznamPodatkovProjekta);
                } else if (uporabnisko_ime.equals(clan1)) {
                    listaMojihProjektov.add(seznamPodatkovProjekta);
                } else if (uporabnisko_ime.equals(clan2)) {
                    listaMojihProjektov.add(seznamPodatkovProjekta);
                } else if (uporabnisko_ime.equals(clan3)) {
                    listaMojihProjektov.add(seznamPodatkovProjekta);
                } else if (uporabnisko_ime.equals(clan4)) {
                    listaMojihProjektov.add(seznamPodatkovProjekta);
                } else if (uporabnisko_ime.equals(clan5)) {
                    listaMojihProjektov.add(seznamPodatkovProjekta);
                } else if (uporabnisko_ime.equals(clan6)) {
                    listaMojihProjektov.add(seznamPodatkovProjekta);
                } else if (uporabnisko_ime.equals(clan7)) {
                    listaMojihProjektov.add(seznamPodatkovProjekta);
                } else if (uporabnisko_ime.equals(clan8)) {
                    listaMojihProjektov.add(seznamPodatkovProjekta);
                } else if (uporabnisko_ime.equals(clan9)) {
                    listaMojihProjektov.add(seznamPodatkovProjekta);
                }

            }

            EventQueue.invokeLater(() -> {
                try {
                    ProjektTipVnosa window = new ProjektTipVnosa();
                    window.vnosnoOknoProjekt(listaMojihProjektov);
                    window.getFrame().setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

        }
    }
}