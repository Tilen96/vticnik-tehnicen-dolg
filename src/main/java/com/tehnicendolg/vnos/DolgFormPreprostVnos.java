package com.tehnicendolg.vnos;

import com.intellij.ui.components.JBScrollPane;
import com.tehnicendolg.rest.PostToRest;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDateTime;

public class DolgFormPreprostVnos {

    private JFrame frame;
    private JTextField naziv;
    private JTextField ocena;

    public void vnosnoOkno(String[] poljeProjekta) throws IOException {
        setFrame(new JFrame("Preprost vnos dolga"));
        getFrame().setBounds(500, 100, 550, 600);
        getFrame().setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        getFrame().getContentPane().setLayout(null);

        setNaziv(new JTextField());
        getNaziv().setBounds(128, 28, 250, 20);
        getFrame().getContentPane().add(getNaziv());
        getNaziv().setColumns(10);

        JLabel naziv = new JLabel("Naziv:");
        naziv.setBounds(65, 28, 46, 14);
        getFrame().getContentPane().add(naziv);

        JLabel vzrok = new JLabel("Vzrok:");
        vzrok.setBounds(65, 68, 46, 14);
        getFrame().getContentPane().add(vzrok);

        JComboBox<String> comboBox2 = new JComboBox<String>();
        comboBox2.addItem("tehni\u010Den");
        comboBox2.addItem("organizacijski");
        comboBox2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        comboBox2.setBounds(180, 68, 150, 20);
        getFrame().getContentPane().add(comboBox2);

        JLabel vnos_pomoc1 = new JLabel("\u0028\u003F\u0029");
        vnos_pomoc1.setBounds(335, 68, 15, 20);
        vnos_pomoc1.setToolTipText("Izberite vzrok, ki je povzro\u010Dil nastanek dolga.\n\n" +
                "Tehni\u010Den vzrok:\n" +
                "izvorna koda ali oblikovanje je preve\u010D kompleksno za nadaljnji razvoj.\n" +
                "Namerne odlo\u010Ditve za vpeljavo tehni\u010Dnega dolga, da se izognemo dolgim\n" +
                "in kompleksnim re\u0161itvam. Lahko gre za motivacijsko te\u017Eavo ali re\u0161itev,\n" +
                "ki omogo\u010Da nadaljnjo delovanje programske opreme, ki se razvija.\n\n" +
                "Organizacijski vzrok:\n" +
                "Informacijska re\u0161itev ali funkcionalnost, mora biti pripravljena za prikaz\n" +
                "delovanja. Zaradi plana razvijalnega podjetja razvijalci vpeljejo tehni\u010Den\n" +
                "dolg, saj ni dovolj \u010Dasa za pravilno implementacijo.");
        getFrame().getContentPane().add(vnos_pomoc1);

        JLabel opis = new JLabel("Opis:");
        opis.setBounds(65, 115, 46, 14);
        getFrame().getContentPane().add(opis);

        JTextArea opisArea = new JTextArea();
        opisArea.setBounds(126, 115, 300, 100);

        JLabel vnos_pomoc2 = new JLabel("\u0028\u003F\u0029");
        vnos_pomoc2.setBounds(431, 115, 15, 20);
        vnos_pomoc2.setToolTipText("Opi\u0161ite dolg, njegove posledice in prepoznane simptome.");
        getFrame().getContentPane().add(vnos_pomoc2);

        JBScrollPane scroll1 = new JBScrollPane(opisArea,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll1.setBounds(126, 115, 300, 100);

        getFrame().getContentPane().add(scroll1);

        JLabel Obseg = new JLabel("Obseg:");
        Obseg.setBounds(65, 225, 46, 16);
        getFrame().getContentPane().add(Obseg);

        JTextArea obseg = new JTextArea();
        obseg.setBounds(126, 225, 300, 100);

        JLabel vnos_pomoc3 = new JLabel("\u0028\u003F\u0029");
        vnos_pomoc3.setBounds(431, 225, 15, 20);
        vnos_pomoc3.setToolTipText("Kaj dolg zajema in kje ga najdemo,\nseznam datotek, razredov,\nmetod, dokumentov, strani...");
        getFrame().getContentPane().add(vnos_pomoc3);

        JBScrollPane scroll2 = new JBScrollPane(obseg,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll2.setBounds(126, 225, 300, 100);

        getFrame().getContentPane().add(scroll2);

        JButton btnClear = new JButton("Po\u010Disti");

        btnClear.setBounds(312, 500, 89, 23);
        getFrame().getContentPane().add(btnClear);

        setOcena(new JTextField());
        getOcena().setBounds(128, 350, 100, 20);
        getFrame().getContentPane().add(getOcena());
        getOcena().setColumns(10);

        JLabel ocena = new JLabel("Ocena:");
        ocena.setBounds(65, 350, 46, 14);
        getFrame().getContentPane().add(ocena);

        String tip_ocene = null;
        if(poljeProjekta[2].equals("število dni")) {
            tip_ocene = "\u0161tevilo dni";
        } else if(poljeProjekta[2].equals("€")) {
            tip_ocene = "\u20AC";
        }

        JLabel ocena_tip = new JLabel(tip_ocene);
        ocena_tip.setBounds(230, 350, 70, 14);
        getFrame().getContentPane().add(ocena_tip);

        JLabel vnos_pomoc4 = new JLabel("\u0028\u003F\u0029");
        vnos_pomoc4.setBounds(305, 350, 15, 20);
        vnos_pomoc4.setToolTipText("Ocenite koli\u010Dino dolga.");
        getFrame().getContentPane().add(vnos_pomoc4);

        JLabel kriticnost = new JLabel("Kriti\u010Dnost:");
        kriticnost.setBounds(65, 395, 67, 14);
        getFrame().getContentPane().add(kriticnost);

        JComboBox<String> comboBox = new JComboBox<String>();
        comboBox.addItem("blokada");
        comboBox.addItem("kriti\u010Dna");
        comboBox.addItem("visoka");
        comboBox.addItem("srednja");
        comboBox.addItem("nizka");
        comboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        comboBox.setBounds(180, 395, 150, 20);
        getFrame().getContentPane().add(comboBox);

        JLabel vnos_pomoc5 = new JLabel("\u0028\u003F\u0029");
        vnos_pomoc5.setBounds(335, 395, 15, 20);
        vnos_pomoc5.setToolTipText("Ocenite kriti\u010Dnost dolga.");
        getFrame().getContentPane().add(vnos_pomoc5);

        JLabel status = new JLabel("Status:");
        status.setBounds(65, 435, 67, 14);
        getFrame().getContentPane().add(status);

        JComboBox<String> comboBox1 = new JComboBox<String>();
        comboBox1.addItem("odprt");
        comboBox1.addItem("v delu");
        comboBox1.addItem("ponovno odprt");
        comboBox1.addItem("re\u0161eno");
        comboBox1.addItem("zaklju\u010Deno");
        comboBox1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        comboBox1.setBounds(180, 435, 150, 20);
        getFrame().getContentPane().add(comboBox1);

        JLabel vnos_pomoc6 = new JLabel("\u0028\u003F\u0029");
        vnos_pomoc6.setBounds(335, 435, 15, 20);
        vnos_pomoc6.setToolTipText("Ocenite status dolga.");
        getFrame().getContentPane().add(vnos_pomoc6);

        JButton btnSubmit = new JButton("Shrani");

        btnSubmit.setBackground(Color.darkGray);
        btnSubmit.setForeground(Color.white);
        btnSubmit.setBounds(65, 500, 89, 23);
        getFrame().getContentPane().add(btnSubmit);

        PostToRest post = new PostToRest();

        btnSubmit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if (getNaziv().getText().isEmpty() || (opisArea.getText().isEmpty()) || (obseg.getText().isEmpty()) || (comboBox.getSelectedIndex() == -1) || (comboBox1.getSelectedIndex() == -1) || (comboBox2.getSelectedIndex() == -1)) {
                    JOptionPane.showMessageDialog(null, "Manjkajo\u010Di podatki");
                }else
                {
                    String json = "{\"naziv\":\""+getNaziv().getText()+"\", " +
                            "\"vzrok\":\""+comboBox2.getSelectedItem().toString()+"\"," +
                            " \"tip\": "+null+", " +
                            "\"podtip\": "+null+"," +
                            " \"opis\":\""+opisArea.getText()+"\", " +
                            "\"obseg\":\""+obseg.getText()+"\", " +
                            "\"ocena\":\""+getOcena().getText()+"\", " +
                            "\"kriticnost\":\""+comboBox.getSelectedItem().toString()+"\"," +
                            " \"status\":\""+comboBox1.getSelectedItem().toString()+"\", " +
                            "\"datum\":\""+LocalDateTime.now()+"\", " +
                            "\"id_projekt\":\""+poljeProjekta[0]+"\", " +
                            "\"clan\":\""+poljeProjekta[3]+"\"," +
                            " \"clan1\":\""+poljeProjekta[4]+"\", " +
                            "\"clan2\":\""+poljeProjekta[5]+"\", " +
                            "\"clan3\":\""+poljeProjekta[6]+"\", " +
                            "\"clan4\":\""+poljeProjekta[7]+"\"," +
                            " \"clan5\":\""+poljeProjekta[8]+"\", " +
                            "\"clan6\":\""+poljeProjekta[9]+"\", " +
                            "\"clan7\":\""+poljeProjekta[10]+"\", " +
                            "\"clan8\":\""+poljeProjekta[11]+"\", " +
                            "\"clan9\":\""+poljeProjekta[12]+"\"}";
                    try {
                        post.shrani(json);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                JOptionPane.showMessageDialog(null, "Dolg shranjen");

            }
        });

        btnClear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                getNaziv().setText(null);
                obseg.setText(null);
                opisArea.setText(null);
                getOcena().setText(null);

            }
        });


    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JTextField getNaziv() {
        return naziv;
    }

    public void setNaziv(JTextField naziv) {
        this.naziv = naziv;
    }

    public JTextField getOcena() {
        return ocena;
    }

    public void setOcena(JTextField ocena) {
        this.ocena = ocena;
    }
}
