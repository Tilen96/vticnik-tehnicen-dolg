package com.tehnicendolg.vnos;

import com.intellij.ui.components.JBScrollPane;
import com.tehnicendolg.rest.PostToRest;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDateTime;

public class DolgFormNapredenVnos {

    private JFrame frame;
    private JTextField naziv;
    private JTextField ocena;

    public void vnosnoOkno(String[] poljeProjekta) throws IOException {
        setFrame(new JFrame("Preprost vnos dolga"));
        getFrame().setBounds(500, 100, 550, 600);
        getFrame().setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        getFrame().getContentPane().setLayout(null);

        setNaziv(new JTextField());
        getNaziv().setBounds(128, 20, 250, 20);
        getFrame().getContentPane().add(getNaziv());
        getNaziv().setColumns(10);

        JLabel naziv = new JLabel("Naziv:");
        naziv.setBounds(65, 20, 46, 14);
        getFrame().getContentPane().add(naziv);

        JLabel tip = new JLabel("Tip:");
        tip.setBounds(65, 50, 46, 14);
        getFrame().getContentPane().add(tip);

        JComboBox<String> comboBox2 = new JComboBox<String>();
        comboBox2.addItem("na\u010Drtovalski dolg");
        comboBox2.addItem("arhitekturni dolg");
        comboBox2.addItem("dokumentacijski dolg");
        comboBox2.addItem("dolg testiranja");
        comboBox2.addItem("dolg programske kode");
        comboBox2.addItem("dolg napak v programu");
        comboBox2.addItem("dolg nepopolnih zahtev projekta");
        comboBox2.addItem("infrastrukturni dolg");
        comboBox2.addItem("\u010Dlove\u0161ki dolg");
        comboBox2.addItem("dolg pomanjkanja avtomatskih testov");
        comboBox2.addItem("procesni dolg");
        comboBox2.addItem("dolg prevajanja projekta");
        comboBox2.addItem("dolg menjave spletnih storitev");
        comboBox2.addItem("dolg uporabnosti");
        comboBox2.addItem("dolg verzij programa");
        comboBox2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        comboBox2.setBounds(128, 50, 300, 20);
        getFrame().getContentPane().add(comboBox2);

        JLabel vnos_pomoc1 = new JLabel("\u0028\u003F\u0029");
        vnos_pomoc1.setBounds(433, 50, 15, 20);
        vnos_pomoc1.setToolTipText("Izberite tip.");
        getFrame().getContentPane().add(vnos_pomoc1);

        JLabel podtip = new JLabel("Podtip:");
        podtip.setBounds(65, 80, 46, 14);
        getFrame().getContentPane().add(podtip);

        JComboBox<String> comboBox3 = new JComboBox<String>();
        comboBox2.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                if(comboBox2.getSelectedIndex() == 0) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("pomanjkljiva koda");
                    comboBox3.addItem("kompleksni razredi ali metode");
                    comboBox3.addItem("ne\u010Disto\u010Da");
                    comboBox3.addItem("pomanjkljiva oblikovna navodila");
                } else if(comboBox2.getSelectedIndex() == 1) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("pomanjkljiva arhitektura");
                    comboBox3.addItem("arhitekturni anti vzorci");
                    comboBox3.addItem("kompleksne arhitekturne vedenjske odvisnosti");
                    comboBox3.addItem("kr\u0161itve dobre arhitekturne prakse");
                    comboBox3.addItem("arhitekturna vpra\u0161anja skladnosti");
                    comboBox3.addItem("te\u017Eave s kakovostjo strukture na ravni sistema");
                } else if(comboBox2.getSelectedIndex() == 2) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("zastarela dokumentacija");
                    comboBox3.addItem("nepopolna dokumentacija");
                    comboBox3.addItem("nezadostna dokumentacija");
                    comboBox3.addItem("pomanjkanje komentarjev");
                } else if(comboBox2.getSelectedIndex() == 3) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("nizka kodna pokritost");
                    comboBox3.addItem("odpravljanje testiranja");
                    comboBox3.addItem("pomanjkanje testov");
                    comboBox3.addItem("pomanjkanje avtomatskih testov");
                    comboBox3.addItem("napake nenajdene s strani testov");
                    comboBox3.addItem("dragi testi");
                    comboBox3.addItem("napake v planu testiranja");
                } else if(comboBox2.getSelectedIndex() == 4) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("nizko kakovostna koda");
                    comboBox3.addItem("podvojena koda");
                    comboBox3.addItem("kr\u0161itve pisanja kode");
                    comboBox3.addItem("kompleksna koda");
                } else if(comboBox2.getSelectedIndex() == 5) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("napake/hro\u0161\u010Di");
                } else if(comboBox2.getSelectedIndex() == 6) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("prekomerno in\u017Eenirstvo");
                } else if(comboBox2.getSelectedIndex() == 7) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("zastarela tehnologija");
                    comboBox3.addItem("zastarela podporna orodja");
                    comboBox3.addItem("pomanjkanje stalnega povezovanja");
                    comboBox3.addItem("pomanjkanje avtomatskega uvajanja");
                    comboBox3.addItem("slabo na\u010Drtovanje izdajanja");
                } else if(comboBox2.getSelectedIndex() == 8) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("\u010Dlove\u0161ki dolg");
                } else if(comboBox2.getSelectedIndex() == 9) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("dolg pomanjkanja avtomatskih testov");
                } else if(comboBox2.getSelectedIndex() == 10) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("procesni dolg");
                } else if(comboBox2.getSelectedIndex() == 11) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("slabe odvisnosti");
                    comboBox3.addItem("ro\u010Dni postopek izdelave");
                    comboBox3.addItem("napaka avtomatske izdelave");
                    comboBox3.addItem("ro\u010Dni postopek izdelave");
                    comboBox3.addItem("viden dolg prevajanja");
                } else if(comboBox2.getSelectedIndex() == 12) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("dolg menjave spletnih storitev");
                } else if(comboBox2.getSelectedIndex() == 13) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("dolg uporabnosti");
                }  else if(comboBox2.getSelectedIndex() == 14) {
                    comboBox3.removeAllItems();
                    comboBox3.addItem("nepotrebno razvejanje kode");
                    comboBox3.addItem("podpora za ve\u010D razli\u010Dic");
                }
            }
        });
        comboBox3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        comboBox3.setBounds(128, 80, 300, 20);
        getFrame().getContentPane().add(comboBox3);

        JLabel vnos_pomoc = new JLabel("\u0028\u003F\u0029");
        vnos_pomoc.setBounds(433, 80, 15, 20);
        vnos_pomoc.setToolTipText("Izberite podtip.");
        getFrame().getContentPane().add(vnos_pomoc);

        JLabel opis = new JLabel("Opis:");
        opis.setBounds(65, 115, 46, 14);
        getFrame().getContentPane().add(opis);

        JTextArea opisArea = new JTextArea();
        opisArea.setBounds(126, 115, 300, 100);

        JLabel vnos_pomoc2 = new JLabel("\u0028\u003F\u0029");
        vnos_pomoc2.setBounds(431, 115, 15, 20);
        vnos_pomoc2.setToolTipText("Opi\u0161ite dolg, njegove posledice in prepoznane simptome.");
        getFrame().getContentPane().add(vnos_pomoc2);

        JBScrollPane scroll1 = new JBScrollPane(opisArea,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll1.setBounds(126, 115, 300, 100);

        getFrame().getContentPane().add(scroll1);

        JLabel Obseg = new JLabel("Obseg:");
        Obseg.setBounds(65, 225, 46, 16);
        getFrame().getContentPane().add(Obseg);

        JTextArea obseg = new JTextArea();
        obseg.setBounds(126, 225, 300, 100);

        JLabel vnos_pomoc3 = new JLabel("\u0028\u003F\u0029");
        vnos_pomoc3.setBounds(431, 225, 15, 20);
        vnos_pomoc3.setToolTipText("Kaj dolg zajema in kje ga najdemo,\nseznam datotek, razredov,\nmetod, dokumentov, strani...");
        getFrame().getContentPane().add(vnos_pomoc3);

        JBScrollPane scroll2 = new JBScrollPane(obseg,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll2.setBounds(126, 225, 300, 100);

        getFrame().getContentPane().add(scroll2);

        JButton btnClear = new JButton("Po\u010Disti");

        btnClear.setBounds(312, 500, 89, 23);
        getFrame().getContentPane().add(btnClear);

        setOcena(new JTextField());
        getOcena().setBounds(128, 350, 100, 20);
        getFrame().getContentPane().add(getOcena());
        getOcena().setColumns(10);

        JLabel ocena = new JLabel("Ocena:");
        ocena.setBounds(65, 350, 46, 14);
        getFrame().getContentPane().add(ocena);

        String tip_ocene = null;
        if(poljeProjekta[2].equals("število dni")) {
            tip_ocene = "\u0161tevilo dni";
        } else if(poljeProjekta[2].equals("€")) {
            tip_ocene = "\u20AC";
        }

        JLabel ocena_tip = new JLabel(tip_ocene);
        ocena_tip.setBounds(230, 350, 70, 14);
        getFrame().getContentPane().add(ocena_tip);

        JLabel vnos_pomoc4 = new JLabel("\u0028\u003F\u0029");
        vnos_pomoc4.setBounds(305, 350, 15, 20);
        vnos_pomoc4.setToolTipText("Ocenite koli\u010Dino dolga.");
        getFrame().getContentPane().add(vnos_pomoc4);

        JLabel kriticnost = new JLabel("Kriti\u010Dnost:");
        kriticnost.setBounds(65, 395, 67, 14);
        getFrame().getContentPane().add(kriticnost);

        JComboBox<String> comboBox = new JComboBox<String>();
        comboBox.addItem("blokada");
        comboBox.addItem("kriti\u010Dna");
        comboBox.addItem("visoka");
        comboBox.addItem("srednja");
        comboBox.addItem("nizka");
        comboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        comboBox.setBounds(180, 395, 150, 20);
        getFrame().getContentPane().add(comboBox);

        JLabel vnos_pomoc5 = new JLabel("\u0028\u003F\u0029");
        vnos_pomoc5.setBounds(335, 395, 15, 20);
        vnos_pomoc5.setToolTipText("Ocenite kriti\u010Dnost dolga.");
        getFrame().getContentPane().add(vnos_pomoc5);

        JLabel status = new JLabel("Status:");
        status.setBounds(65, 435, 67, 14);
        getFrame().getContentPane().add(status);

        JComboBox<String> comboBox1 = new JComboBox<String>();
        comboBox1.addItem("odprt");
        comboBox1.addItem("v delu");
        comboBox1.addItem("ponovno odprt");
        comboBox1.addItem("re\u0161eno");
        comboBox1.addItem("zaklju\u010Deno");
        comboBox1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        comboBox1.setBounds(180, 435, 150, 20);
        getFrame().getContentPane().add(comboBox1);

        JLabel vnos_pomoc6 = new JLabel("\u0028\u003F\u0029");
        vnos_pomoc6.setBounds(335, 435, 15, 20);
        vnos_pomoc6.setToolTipText("Ocenite status dolga.");
        getFrame().getContentPane().add(vnos_pomoc6);

        JButton btnSubmit = new JButton("Shrani");

        btnSubmit.setBackground(Color.darkGray);
        btnSubmit.setForeground(Color.white);
        btnSubmit.setBounds(65, 500, 89, 23);
        getFrame().getContentPane().add(btnSubmit);

        PostToRest post = new PostToRest();

        btnSubmit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if (getNaziv().getText().isEmpty() || (opisArea.getText().isEmpty()) || (obseg.getText().isEmpty()) || (comboBox.getSelectedIndex() == -1) || (comboBox1.getSelectedIndex() == -1) || (comboBox2.getSelectedIndex() == -1)) {
                    JOptionPane.showMessageDialog(null, "Manjkajo\u010Di podatki");
                }else
                {
                    String json = "{\"naziv\":\""+getNaziv().getText()+"\", " +
                            "\"vzrok\": "+null+"," +
                            " \"tip\": \""+comboBox2.getSelectedItem().toString()+"\", " +
                            "\"podtip\": \""+comboBox3.getSelectedItem().toString()+"\"," +
                            " \"opis\":\""+opisArea.getText()+"\", " +
                            "\"obseg\":\""+obseg.getText()+"\", " +
                            "\"ocena\":\""+getOcena().getText()+"\", " +
                            "\"kriticnost\":\""+comboBox.getSelectedItem().toString()+"\"," +
                            " \"status\":\""+comboBox1.getSelectedItem().toString()+"\", " +
                            "\"datum\":\""+LocalDateTime.now()+"\", " +
                            "\"id_projekt\":\""+poljeProjekta[0]+"\", " +
                            "\"clan\":\""+poljeProjekta[3]+"\"," +
                            " \"clan1\":\""+poljeProjekta[4]+"\", " +
                            "\"clan2\":\""+poljeProjekta[5]+"\", " +
                            "\"clan3\":\""+poljeProjekta[6]+"\", " +
                            "\"clan4\":\""+poljeProjekta[7]+"\"," +
                            " \"clan5\":\""+poljeProjekta[8]+"\", " +
                            "\"clan6\":\""+poljeProjekta[9]+"\", " +
                            "\"clan7\":\""+poljeProjekta[10]+"\", " +
                            "\"clan8\":\""+poljeProjekta[11]+"\", " +
                            "\"clan9\":\""+poljeProjekta[12]+"\"}";
                    try {
                        post.shrani(json);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                JOptionPane.showMessageDialog(null, "Dolg shranjen");

            }
        });

        btnClear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                getNaziv().setText(null);
                obseg.setText(null);
                opisArea.setText(null);
                getOcena().setText(null);

            }
        });

    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JTextField getNaziv() {
        return naziv;
    }

    public void setNaziv(JTextField naziv) {
        this.naziv = naziv;
    }

    public JTextField getOcena() {
        return ocena;
    }

    public void setOcena(JTextField ocena) {
        this.ocena = ocena;
    }
}
