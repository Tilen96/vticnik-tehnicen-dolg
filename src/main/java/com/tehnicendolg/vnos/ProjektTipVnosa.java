package com.tehnicendolg.vnos;

import com.tehnicendolg.vnos.DolgFormNapredenVnos;
import com.tehnicendolg.vnos.DolgFormPreprostVnos;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

public class ProjektTipVnosa {

    private JFrame frame;

    public void vnosnoOknoProjekt(ArrayList<String[]> listaMojihProjektov) throws IOException {
        setFrame(new JFrame("Projekt in tip vnosa"));
        getFrame().setBounds(500, 250, 500, 300);
        getFrame().setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        getFrame().getContentPane().setLayout(null);


        JLabel projekt_label = new JLabel("Projekt:");
        projekt_label.setBounds(65, 40, 67, 14);
        getFrame().getContentPane().add(projekt_label);

        JComboBox<String> projekt = new JComboBox<String>();
        for (int i = 0; i < listaMojihProjektov.size(); i++) {
            projekt.addItem(listaMojihProjektov.get(i)[1]);
        }
        projekt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        projekt.setBounds(130, 40, 300, 20);
        getFrame().getContentPane().add(projekt);

        JLabel projekt_pomoc = new JLabel("\u0028\u003F\u0029");
        projekt_pomoc.setBounds(435, 40, 15, 20);
        projekt_pomoc.setToolTipText("Izberite projekt v katerega \u017Eelite dodati dolg.");
        getFrame().getContentPane().add(projekt_pomoc);

        JLabel vnos = new JLabel("Izberite tip vnosa:");
        vnos.setBounds(65, 100, 100, 14);
        getFrame().getContentPane().add(vnos);

        JLabel vnos_pomoc = new JLabel("\u0028\u003F\u0029");
        vnos_pomoc.setBounds(435, 100, 15, 20);
        vnos_pomoc.setToolTipText("Na kak\u0161en na\u010Din \u017Eelite vnesti dolg.\nPreprost vnos: vzrok.\nNapreden vnos: tip in podtip.");
        getFrame().getContentPane().add(vnos_pomoc);

        JRadioButton r1=new JRadioButton("preprost");
        JRadioButton r2=new JRadioButton("napreden");
        r1.setBounds(65,120,100,30);
        r2.setBounds(175,120,100,30);
        ButtonGroup bg=new ButtonGroup();
        bg.add(r1);bg.add(r2);
        getFrame().getContentPane().add(r1);
        getFrame().getContentPane().add(r2);

        JButton btnSubmit = new JButton("Potrdi");

        btnSubmit.setBackground(Color.darkGray);
        btnSubmit.setForeground(Color.white);
        btnSubmit.setBounds(65, 200, 89, 23);
        getFrame().getContentPane().add(btnSubmit);


        btnSubmit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if ((projekt.getSelectedIndex() == -1) || (!r1.isSelected() && !r2.isSelected())) {
                    JOptionPane.showMessageDialog(null, "Manjkajo\u010Di podatki");
                } else {
                    //JOptionPane.showMessageDialog(null, "Dolg shranjen");
                    if(r1.isSelected()){
                        DolgFormPreprostVnos preprostVnos = new DolgFormPreprostVnos();
                        try {
                            preprostVnos.vnosnoOkno(listaMojihProjektov.get(projekt.getSelectedIndex()));
                            getFrame().setVisible(false);
                            preprostVnos.getFrame().setVisible(true);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else if(r2.isSelected()){
                        DolgFormNapredenVnos napredenVnos = new DolgFormNapredenVnos();
                        try {
                            napredenVnos.vnosnoOkno(listaMojihProjektov.get(projekt.getSelectedIndex()));
                            getFrame().setVisible(false);
                            napredenVnos.getFrame().setVisible(true);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }

            }
        });
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

}
