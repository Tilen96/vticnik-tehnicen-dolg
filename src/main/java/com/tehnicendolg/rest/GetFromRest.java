package com.tehnicendolg.rest;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class GetFromRest {

    private static HttpURLConnection con;

    public JSONArray pridobi_podatke(String url){

        try {

            URL myurl = null;
            try {
                myurl = new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                con = (HttpURLConnection) myurl.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                con.setRequestMethod("GET");
            } catch (ProtocolException e) {
                e.printStackTrace();
            }

            StringBuilder content = null;

            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()))) {

                String line;
                content = new StringBuilder();

                while ((line = in.readLine()) != null) {
                    content.append(line);
                    content.append(System.lineSeparator());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONArray json = null;

            try {
                json = new JSONArray(content.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return  json;

        } finally {

            con.disconnect();
        }

    }
}
